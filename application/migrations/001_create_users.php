<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Migration_create_users extends CI_Migration {
public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '128',
            ),
            'nama' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'gender' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'no_telp' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'pekerjaan' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'foto' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),

        ));
        $this->dbforge->add_key('id');
        $this->dbforge->create_table('user');
    }
    public function down() {
        $this->dbforge->drop_table('user');
    }
}