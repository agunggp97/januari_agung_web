  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include("sidebar.php");?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user text-black"></i>
        User List
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-calendar"></i><script type='text/javascript'>

        var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];

        var date = new Date();

        var day = date.getDate();

        var month = date.getMonth();

        var thisDay = date.getDay(),

            thisDay = myDays[thisDay];

        var yy = date.getYear();

        var year = (yy < 1000) ? yy + 1900 : yy;

        document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);

        </script></a></li>
      </ol>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->

        </div>

      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="block">
			<div class="block-title">
				<div class="block-options pull-right">

				</div>
				<h2><strong>Data User</strong></h2>
			</div>
      <div class="box">

        <!-- /.box-header -->
        <div class="box-body">
          <?php if ($this->session->flashdata('sukses') != "") {?>
           <div class="alert alert-info" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><font color="white"><?php echo $this->session->flashdata('sukses');?></font></strong>
            </div>
          <?php }else {?>
          <?php }?>
          <?php if ($this->session->flashdata('hapus') != "") {?>
           <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><font color="white"><?php echo $this->session->flashdata('hapus');?></font></strong>
            </div>
          <?php }else {?>
          <?php }?>          
          <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>E-mail</th>
              <th>Gender</th>
              <th>Nomor Telepon</th>
              <th>Pekerjaan</th>
			        <th>Foto</th>
              <th>Aksi</th>
            </tr>
            </thead>
            <tbody>

            <?php $no = 1;
                  foreach($user as $h){ ?>
            <tr>
              <td><?php echo $no++;?></td>
              <td><?php echo $h->nama;?></td>
              <td><?php echo $h->email;?></td>
              <td><?php echo $h->gender;?></td>
              <td><?php echo $h->no_telp;?></td>
              <td><?php echo $h->pekerjaan;?></td>
              <td><img width="75" height="100" src='<?=base_url()?>images/<?=$h->foto;?>'></td>
              <td align="center"><a href="<?php echo site_url('user/delete_user/'.$h->id); ?>  " data-original-title="Hapus" data-toggle="tooltip" onclick="if(confirm('Apakah anda yakin mengapus data ini?')) return true; else return false;" data-placement="top" class="btn btn-xs menu-icon btn-danger"> <i class="fa fa-trash"></i> </a></td>
              </tr>
      <?php }?>
            </tfoot>
          </table>
        </div>
      </div>
        <!-- /.box-body -->
      </div>
		</div>

        </section>
        <!-- /.Left col -->
        
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->