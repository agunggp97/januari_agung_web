<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>/images/user.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>nama</p>
          <small>role</small>
          </div>
      </div>
      <br/>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li>
          <a href="<?php echo site_url('user')?>">
            <i class="fa fa-user"></i> <span>User List</span>
          </a>
        </li>
        <li>
          <a href="<?php echo site_url('user/form')?>">
            <i class="fa fa-file-text-o"></i>
            <span>User Form</span>
          </a>
        </li>

        </ul>
      
    </section>