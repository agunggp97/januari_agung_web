  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include("sidebar.php");?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-file-text-o text-black"></i>
        User Form
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-calendar"></i><script type='text/javascript'>

        var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];

        var date = new Date();

        var day = date.getDate();

        var month = date.getMonth();

        var thisDay = date.getDay(),

            thisDay = myDays[thisDay];

        var yy = date.getYear();

        var year = (yy < 1000) ? yy + 1900 : yy;

        document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);

        </script></a></li>
      </ol>
    </section>
    <!-- Main content -->

    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->

        </div>

      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>Register User</strong></h3><br/>
                  <font size="2">Untuk kolom yang bertanda <sup style="color:red">*</sup> = wajib diisi</font>
            </div>
            <!--hei Mulai-->
            <script>
            function tampilkanPreview(gambar,idpreview){
//                membuat objek gambar
                var gb = gambar.files;
                
//                loop untuk merender gambar
                for (var i = 0; i < gb.length; i++){
//                    bikin variabel
                    var gbPreview = gb[i];
                    var imageType = /image.*/;
                    var preview=document.getElementById(idpreview);            
                    var reader = new FileReader();
                    
                    if (gbPreview.type.match(imageType)) {
//                        jika tipe data sesuai
                        preview.file = gbPreview;
                        reader.onload = (function(element) { 
                            return function(e) { 
                                element.src = e.target.result; 
                            }; 
                        })(preview);
 
    //                    membaca data URL gambar
                        reader.readAsDataURL(gbPreview);
                    }else{
//                        jika tipe data tidak sesuai
                        alert("Tipe file tidak sesuai. Khusus gambar.");
                        
                    }
                   
                }    
            }
        </script>
        <!--hei udah-->
              <?php echo form_open_multipart('User/register');?>
              <form action="<?php echo site_url('User/register');?>" method="POST">
               
            <div class="box-body">
              <div class="row">
                <div class="col-md-11">
                  <font color="red">
          <?php echo show_err_msg($this->session->flashdata('error_msg'));?></font>
          <div style="color: red;"><?php echo (isset($message))? $message : ""; ?></div>
                  <div class="form-group">
                  <label class="col-md-3 control-label">E-mail <sup style="color:red">*</sup></label>
                    <input type="text" class="form-control" name="email" placeholder="Your E-mail" required="">
                  </div>
                  <div class="form-group">
                  <label class="col-md-3 control-label">Password <sup style="color:red">*</sup></label>
                    <input type="password" class="form-control" name="password" placeholder="Type Your Password" required="">
                  </div>
                  <div class="form-group">
                  <label class="col-md-3 control-label">Retype Password <sup style="color:red">*</sup></label>
                    <input type="password" class="form-control" name="repassword" placeholder="Retype Your Password" required="">
                  </div>
                  <div class="form-group">
                  <label class="col-md-3 control-label">Name <sup style="color:red">*</sup></label>
                    <input type="text" class="form-control" name="name" placeholder="Your Name" required="">
                  </div>
                  <div class="form-group">
                  <label class="col-md-2 control-label">Gender <sup style="color:red">*</sup></label>
                    <div class="col-md-2">
                    <input type="radio" class="flat-blue" name="gender" value="Laki-Laki" required=""> <strong>Laki-Laki</strong>
                    </div>
                    <div class="col-md-2">
                    <input type="radio" class="flat-blue" name="gender" value="Perempuan"> <strong>Perempuan</strong>
                    </div>
                  </div><br/><br/>
                  <div class="form-group">
                  <label class="col-md-3 control-label">No Telp <sup style="color:red">*</sup></label>
                    <input type="number" class="form-control" name="notelp" placeholder="Your Telp Number" required="">
                  </div>
                  <div class="form-group">
                  <label class="col-md-3 control-label">Pekerjaan<sup style="color:red">*</sup></label>
                    <select name="job" class="form-control select2" style="width: 100%;" required>
                      <option selected="selected">- Choose -</option>
                      <option value="Karyawan Swasta">Karyawan Swasta</option>
                      <option value="Pegawai Negeri">Pegawai Negeri</option>
                      <option value="Belum Bekerja">Belum Bekerja</option>
                    </select>
                  </div>
				         <div class="form-group">
                  <font color="red">
          <?php echo $this->session->flashdata('error_msg');?></font> <font color="red">
          <?php echo $this->session->flashdata('masuk_msg');?></font>
                  <label class="col-md-3 control-label">Upload Foto <sup style="color:red">*</sup></label>
                    <input type="file" accept="image/*" id="exampleInputFile" name="foto"  onchange="tampilkanPreview(this,'preview')">
                  </div>
                  <div class="form-group">
                  <label class="col-md-3 control-label"> </label>
                  <img id="preview" src="" alt="" width="250px"/>
                  </div>
                  <div class="row form-group text-center">
					            <input type="submit" value="Register" class="btn btn-primary">
				          </div>
                </div>
              </div>
            </div>
          </form>

          </div>
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>

