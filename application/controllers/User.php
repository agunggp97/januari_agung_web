<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
		{
			parent::__construct();
			//load helpernya
			$this->load->model('m_data');
		}

	public function index(){
		$data['user']=$this->m_data->ambilData($tbl="user")->result();
		$this->load->view('header');
		$this->load->view('user_list',$data);
		$this->load->view('footer');
	}
	public function form(){
		$this->load->view('header');
		$this->load->view('user_form');
		$this->load->view('footer');
	}
	public function register(){
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');	
		$this->form_validation->set_rules('name', 'Nama', 'required');	
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|matches[repassword]');	
		$this->form_validation->set_rules('repassword', 'Retype Password', 'required|min_length[5]|matches[password]');	
		$this->form_validation->set_rules('gender', '', 'required');	
		$this->form_validation->set_rules('notelp',  'Your Telp Number', 'required|numeric');	
		$this->form_validation->set_rules('job', '', 'required');	
 
		if ($this->form_validation->run() == TRUE) {
		$email = $this->input->post('email');
		$name = $this->input->post('name');
		$notelp = $this->input->post('notelp');
		$pass = md5($this->input->post('password'));
		$job = $this->input->post('job');
		$gender = $this->input->post('gender');

		$config['upload_path'] = 'images/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['file_name'] = $_FILES['foto']['name'];
		if(!empty($_FILES['foto']['name']))
	{
             
				
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('foto')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{
                	 $this->session->set_flashdata('error_msg', 'Format file yang diterima hanya Gambar (jpg dan png).');
				   redirect('user/form');
                }
            }
            else{
			$picture = "user.jpg";
		}
		$data = array(
			'email' => $email,
			'password' => $pass,
			'nama' => $name,
			'gender' => $gender,
			'no_telp' => $notelp,
			'pekerjaan' => $job,
			'foto' => $picture,
			);

		$this->m_data->register_user($data,'user');
		$this->session->set_flashdata('sukses','Registrasi Berhasil');
		redirect('user');
	}else {
		$this->session->set_flashdata('error_msg', validation_errors());
		redirect('user/form');
	}
		
	}
	public function delete_user($id){
		$where = array('id' => $id);
		$this->m_data->hapus_data($where,'user');
		$this->session->set_flashdata('hapus','Data berhasil Dihapus');
		redirect('user');
	}
}


	

